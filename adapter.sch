EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Pico adapter for Atlas"
Date "2022-04-30"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 6600 2250
NoConn ~ 7200 2650
NoConn ~ 7200 2750
NoConn ~ 7200 2950
NoConn ~ 7200 3050
NoConn ~ 7200 3350
NoConn ~ 7200 3450
NoConn ~ 7200 4250
NoConn ~ 7200 4350
NoConn ~ 5600 4150
NoConn ~ 5600 4050
NoConn ~ 5600 3950
NoConn ~ 5600 3850
NoConn ~ 5600 3150
NoConn ~ 5600 2950
$Comp
L power:GND #PWR0101
U 1 1 60D1062F
P 6300 5750
F 0 "#PWR0101" H 6300 5500 50  0001 C CNN
F 1 "GND" H 6305 5577 50  0000 C CNN
F 2 "" H 6300 5750 50  0001 C CNN
F 3 "" H 6300 5750 50  0001 C CNN
	1    6300 5750
	1    0    0    -1  
$EndComp
Text GLabel 5250 2650 0    50   Input ~ 0
D16_PI_RX
Wire Wire Line
	5250 2650 5600 2650
Text GLabel 5250 2800 0    50   Input ~ 0
D15_PI_TX
Wire Wire Line
	5250 2800 5600 2800
Wire Wire Line
	5600 2800 5600 2750
Text GLabel 7500 3750 2    50   Input ~ 0
C15_PI_CS
Text GLabel 7500 3850 2    50   Input ~ 0
F15_PI_MISO
Text GLabel 7500 3950 2    50   Input ~ 0
F16_PI_MOSI
Text GLabel 7500 4050 2    50   Input ~ 0
F13_PI_CLK
Text GLabel 2550 3100 0    50   Input ~ 0
TDI
Text GLabel 2550 2900 0    50   Input ~ 0
TCK
Text GLabel 2550 3000 0    50   Input ~ 0
TDO
Text GLabel 2550 3200 0    50   Input ~ 0
TMS
Text GLabel 2550 2700 0    50   Input ~ 0
D16_PI_RX
Text GLabel 2550 2600 0    50   Input ~ 0
D15_PI_TX
Wire Wire Line
	6400 5550 6300 5550
Connection ~ 6300 5550
Connection ~ 6400 5550
Wire Wire Line
	6700 5550 6600 5550
Connection ~ 6600 5550
Wire Wire Line
	6300 5550 6300 5750
$Comp
L power:GND #PWR01
U 1 1 60D743E1
P 3550 5000
F 0 "#PWR01" H 3550 4750 50  0001 C CNN
F 1 "GND" H 3555 4827 50  0000 C CNN
F 2 "" H 3550 5000 50  0001 C CNN
F 3 "" H 3550 5000 50  0001 C CNN
	1    3550 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2900 2850 2900
Wire Wire Line
	2550 3200 2850 3200
Wire Wire Line
	2550 3000 2850 3000
Wire Wire Line
	2550 3100 2850 3100
Wire Wire Line
	3550 4700 3550 5000
$Comp
L power:GND #PWR0102
U 1 1 6231FD08
P 1550 3800
F 0 "#PWR0102" H 1550 3550 50  0001 C CNN
F 1 "GND" H 1555 3627 50  0000 C CNN
F 2 "" H 1550 3800 50  0001 C CNN
F 3 "" H 1550 3800 50  0001 C CNN
	1    1550 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 3300 1550 3800
NoConn ~ 4250 2900
NoConn ~ 4250 3100
NoConn ~ 4250 3200
NoConn ~ 4250 3300
NoConn ~ 4250 3400
NoConn ~ 4250 3500
NoConn ~ 4250 3600
NoConn ~ 3650 4700
Wire Wire Line
	4250 2700 4500 2700
Wire Wire Line
	7500 4050 7200 4050
Wire Wire Line
	7500 3950 7200 3950
Wire Wire Line
	7500 3850 7200 3850
Wire Wire Line
	7500 3750 7200 3750
Wire Wire Line
	6600 4850 6600 5550
Wire Wire Line
	6400 4850 6400 5550
Wire Wire Line
	6300 4850 6300 5550
$Comp
L Connector:Raspberry_Pi_2_3 J1
U 1 1 60CF06AB
P 6400 3550
F 0 "J1" H 6400 5031 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 6400 4940 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 6400 3550 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 6400 3550 50  0001 C CNN
	1    6400 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4850 6200 5550
Connection ~ 6200 5550
Wire Wire Line
	6200 5550 6300 5550
Wire Wire Line
	6000 4850 6000 5550
Wire Wire Line
	6000 5550 6100 5550
Wire Wire Line
	6100 4850 6100 5550
Connection ~ 6100 5550
Wire Wire Line
	6100 5550 6200 5550
Wire Wire Line
	6700 4850 6700 5550
Wire Wire Line
	6400 5550 6500 5550
Wire Wire Line
	6500 4850 6500 5550
Connection ~ 6500 5550
Wire Wire Line
	6500 5550 6600 5550
Wire Wire Line
	2200 3800 1550 3800
Connection ~ 2200 3800
Connection ~ 1550 3800
Text GLabel 7500 3650 2    50   Input ~ 0
MCU_SD_CS
Wire Wire Line
	7200 3650 7500 3650
Text GLabel 5350 3450 0    50   Input ~ 0
MCU_SD_MOSI
Text GLabel 5350 3350 0    50   Input ~ 0
MCU_SD_MISO
Wire Wire Line
	5600 3450 5350 3450
Wire Wire Line
	5600 3350 5350 3350
Text GLabel 5350 3550 0    50   Input ~ 0
MCU_SD_SCK
Wire Wire Line
	5600 3550 5350 3550
Text GLabel 4450 4500 2    50   Input ~ 0
MCU_SD_MISO
Text GLabel 4450 4100 2    50   Input ~ 0
MCU_SD_MOSI
Text GLabel 4450 4200 2    50   Input ~ 0
MCU_SD_SCK
Text GLabel 4450 4400 2    50   Input ~ 0
MCU_SD_CS
Wire Wire Line
	2550 2600 2850 2600
Wire Wire Line
	2850 2700 2550 2700
Wire Wire Line
	4450 4400 4250 4400
Wire Wire Line
	4250 4100 4450 4100
Wire Wire Line
	4250 4200 4450 4200
Wire Wire Line
	4250 4500 4450 4500
Text GLabel 6500 1900 2    50   Input ~ 0
+3.3_SD
Text GLabel 7500 3250 2    50   Input ~ 0
TMS
Wire Wire Line
	7200 3250 7500 3250
Wire Wire Line
	4250 4000 4450 4000
$Comp
L power:+5V #PWR0103
U 1 1 6266FC26
P 6200 2000
F 0 "#PWR0103" H 6200 1850 50  0001 C CNN
F 1 "+5V" H 6215 2173 50  0000 C CNN
F 2 "" H 6200 2000 50  0001 C CNN
F 3 "" H 6200 2000 50  0001 C CNN
	1    6200 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2250 6200 2000
$Comp
L power:+5V #PWR0104
U 1 1 62672FF1
P 4500 2600
F 0 "#PWR0104" H 4500 2450 50  0001 C CNN
F 1 "+5V" H 4515 2773 50  0000 C CNN
F 2 "" H 4500 2600 50  0001 C CNN
F 3 "" H 4500 2600 50  0001 C CNN
	1    4500 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2700 4500 2600
Text GLabel 5350 4250 0    50   Input ~ 0
TDO
Wire Wire Line
	5350 4250 5600 4250
Text GLabel 5350 3050 0    50   Input ~ 0
TCK
Wire Wire Line
	5600 3050 5350 3050
Text GLabel 5350 3750 0    50   Input ~ 0
TDI
Wire Wire Line
	5600 3750 5350 3750
Text GLabel 4450 3000 2    50   Input ~ 0
+3.3_SD
Wire Wire Line
	6500 1900 6500 2250
Wire Wire Line
	4250 3000 4450 3000
Wire Wire Line
	2850 3300 1550 3300
Wire Wire Line
	2850 4300 2200 4300
Wire Wire Line
	2850 3800 2200 3800
$Comp
L MCU_RaspberryPi_and_Boards:Pico Pico1
U 1 1 622E7912
P 3550 3550
F 0 "Pico1" H 3550 4765 50  0000 C CNN
F 1 "Pico" H 3550 4674 50  0000 C CNN
F 2 "RP_PICO:RPi_Pico_SMD_TH_CUSTOM" V 3550 3550 50  0001 C CNN
F 3 "" H 3550 3550 50  0001 C CNN
	1    3550 3550
	1    0    0    -1  
$EndComp
Text GLabel 2600 3600 0    50   Input ~ 0
TFT_RST
Text GLabel 2600 3500 0    50   Input ~ 0
TFT_DC
Text GLabel 2600 3700 0    50   Input ~ 0
TFT_CS
Text GLabel 2750 4000 0    50   Input ~ 0
TFT_MOSI
Text GLabel 2750 3900 0    50   Input ~ 0
TFT_SCK
Text GLabel 1200 4750 2    50   Input ~ 0
+3.3_SD
$Comp
L power:GND #PWR0105
U 1 1 6266CBF1
P 1750 4650
F 0 "#PWR0105" H 1750 4400 50  0001 C CNN
F 1 "GND" H 1755 4477 50  0000 C CNN
F 2 "" H 1750 4650 50  0001 C CNN
F 3 "" H 1750 4650 50  0001 C CNN
	1    1750 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  4750 1200 4750
Wire Wire Line
	950  4650 1750 4650
Text GLabel 1200 4550 2    50   Input ~ 0
TFT_CS
Text GLabel 1200 4450 2    50   Input ~ 0
TFT_RST
Text GLabel 1200 4350 2    50   Input ~ 0
TFT_DC
Text GLabel 1200 4250 2    50   Input ~ 0
TFT_MOSI
Wire Wire Line
	2200 4300 2200 3800
Text GLabel 1200 4150 2    50   Input ~ 0
TFT_SCK
Text GLabel 4300 2400 1    50   Input ~ 0
VBus
Wire Wire Line
	4300 2400 4300 2600
Wire Wire Line
	4300 2600 4250 2600
Text GLabel 1200 4050 2    50   Input ~ 0
VBus
Wire Wire Line
	950  4050 1200 4050
Wire Wire Line
	1200 4150 950  4150
Wire Wire Line
	950  4250 1200 4250
Wire Wire Line
	1200 4350 950  4350
Wire Wire Line
	950  4450 1200 4450
Wire Wire Line
	1200 4550 950  4550
NoConn ~ 3450 4700
Wire Wire Line
	2600 3700 2850 3700
Text GLabel 2750 4100 0    50   Input ~ 0
F15_PI_MISO
Text GLabel 2750 4200 0    50   Input ~ 0
C15_PI_CS
Text GLabel 2750 4400 0    50   Input ~ 0
F13_PI_CLK
Text GLabel 2750 4500 0    50   Input ~ 0
F16_PI_MOSI
Wire Wire Line
	2750 3900 2850 3900
Wire Wire Line
	2750 4000 2850 4000
Wire Wire Line
	2750 4100 2850 4100
Wire Wire Line
	2750 4200 2850 4200
Wire Wire Line
	2750 4400 2850 4400
Wire Wire Line
	2750 4500 2850 4500
Wire Wire Line
	2600 3500 2850 3500
Wire Wire Line
	2600 3600 2850 3600
NoConn ~ 2850 3400
$Comp
L Switch:SW_Push USR_BTN1
U 1 1 626F1A68
P 4550 5150
F 0 "USR_BTN1" H 4550 5435 50  0000 C CNN
F 1 "SW_Push" H 4550 5344 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4550 5350 50  0001 C CNN
F 3 "~" H 4550 5350 50  0001 C CNN
	1    4550 5150
	1    0    0    -1  
$EndComp
Text GLabel 4450 4000 2    50   Input ~ 0
BTN_PIN
Text GLabel 4200 5150 0    50   Input ~ 0
BTN_PIN
Wire Wire Line
	4200 5150 4350 5150
$Comp
L Connector:Conn_01x08_Female TFT1
U 1 1 6268ECC8
P 750 4450
F 0 "TFT1" H 850 4950 50  0000 C CNN
F 1 "Conn_01x08_Female TFT AZ Delivery" H 50  4950 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 750 4450 50  0001 C CNN
F 3 "~" H 750 4450 50  0001 C CNN
	1    750  4450
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_Push RESET1
U 1 1 62711D45
P 4550 5600
F 0 "RESET1" H 4550 5885 50  0000 C CNN
F 1 "SW_Push" H 4550 5794 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4550 5800 50  0001 C CNN
F 3 "~" H 4550 5800 50  0001 C CNN
	1    4550 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 5600 4750 5600
Text GLabel 4450 3600 2    50   Input ~ 0
RST_PIN
Text GLabel 4200 5600 0    50   Input ~ 0
RST_PIN
Wire Wire Line
	4200 5600 4350 5600
$Comp
L power:GND #PWR0106
U 1 1 6277E755
P 5100 4700
F 0 "#PWR0106" H 5100 4450 50  0001 C CNN
F 1 "GND" H 5105 4527 50  0000 C CNN
F 2 "" H 5100 4700 50  0001 C CNN
F 3 "" H 5100 4700 50  0001 C CNN
	1    5100 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3800 4400 3800
Wire Wire Line
	5100 3800 5100 4300
Wire Wire Line
	4250 4300 5100 4300
Connection ~ 5100 4300
Wire Wire Line
	5100 4300 5100 4650
Wire Wire Line
	4250 2800 4400 2800
Wire Wire Line
	4400 2800 4400 3800
Connection ~ 4400 3800
Wire Wire Line
	4400 3800 5100 3800
Wire Wire Line
	4900 5150 4900 4650
Wire Wire Line
	4900 4650 5100 4650
Wire Wire Line
	4750 5150 4850 5150
Connection ~ 5100 4650
Wire Wire Line
	5100 4650 5100 4700
Wire Wire Line
	4850 5600 4850 5150
Connection ~ 4850 5150
Wire Wire Line
	4850 5150 4900 5150
NoConn ~ 4250 3900
Wire Wire Line
	1550 2800 1550 3300
Wire Wire Line
	1550 2800 2850 2800
Connection ~ 1550 3300
Text GLabel 6450 1700 2    50   Input ~ 0
VBus
Wire Wire Line
	6300 2250 6300 1700
Wire Wire Line
	6300 1700 6450 1700
Wire Wire Line
	4450 3600 4250 3600
$EndSCHEMATC
